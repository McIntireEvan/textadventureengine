﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace TextAdventureEngine
{
    class Game
    {
        private String startroom = "";
        private String current = "";
        private Dictionary<String, dynamic> main = new Dictionary<String, dynamic>();
        private Dictionary<String, dynamic> itemD = new Dictionary<String, dynamic>();
        private Dictionary<String, dynamic> currentFile = new Dictionary<String, dynamic>();

        private Dictionary<String, int> states = new Dictionary<String, int>();
        private List<dynamic> playerItems = new List<dynamic>();
        private Dictionary<String, List<dynamic>> items = new Dictionary<String, List<dynamic>>();
        
        public Game()
        {
            startUp();

            if (parseJSONFile(current + ".json").ContainsKey("title"))
            {
                Console.Title = parseJSONFile(current + ".json")["title"];
                Console.WriteLine(parseJSONFile(current + ".json")["title"]);
            }
            if (parseJSONFile(current + ".json").ContainsKey("description"))
            {
                printDescription(current);
            }
        }

        public void gameLoop()
        {
            while (true)
            {
                String[] command = getCommand();
                if(!(command[0].StartsWith("!")))
                {
                    evalCommand(command);
                }
                
            }
        }

        private void evalCommand(String[] command)
        {
            currentFile = parseJSONFile(current + ".json");

            getAllStatechangers(currentFile);

            String action = "";

            if (currentFile.ContainsKey(command[0]))
            {
                action = parseCommand(command, currentFile);
            }
            else
            {
                if(currentFile.ContainsKey("INHERITS"))
                {
                    ArrayList inheritsFrom = currentFile["INHERITS"];

                    foreach (String f in inheritsFrom)
                    {
                        Dictionary<String, dynamic> vals = parseJSONFile(f + ".json");

                        if (vals.ContainsKey(command[0]))
                        {
                            action = parseCommand(command, vals);
                        }
                    }
                } 
                else if (main.ContainsKey(command[0]))
                {
                    action = parseCommand(command, main);
                }

                else
                {
                    if (command[0].Equals(main["invCommand"]))
                    {
                        Console.WriteLine("---Items---");
                        foreach (String s in playerItems)
                        {
                            Console.WriteLine(s);
                        }

                        return;
                    }

                    String errorMessage = (String)main["unexistingCommand"];
                    Console.WriteLine(errorMessage);
                }
                if (action.Equals(""))
                {
                    return;
                }
            }
            
            doCommand(action);
        }

        private String parseCommand(String[] command, Dictionary<String, dynamic> currentRoom)
        {
            String action = "";
            Dictionary<string, int> indexes = new Dictionary<string, int>();
            if ((currentRoom.ContainsKey(command[0])))
            {
                dynamic actionList = currentRoom[command[0]];
                if (command.Length != 1)
                {
                    if (actionList is Dictionary<string, Object>)
                    {
                        for (int i = 1; i < command.Length; i++)
                        {
                            if ((actionList.ContainsKey(command[i])))
                            {
                                actionList = actionList[command[i]];
                            }
                            else
                            {
                                String a = "";
                                foreach (String s in actionList.Keys)
                                {
                                    if (s.StartsWith("$"))
                                    {
                                        a = s;
                                        break;
                                    }
                                }
                                if (a.Equals(""))
                                {
                                    Console.WriteLine(main["unexistingCommand"]);
                                    return "";
                                }
                                actionList = actionList[a];
                                indexes.Add(a, i);
                            }

                        }
                    }
                }
                if (!(actionList is String))
                {
                    Console.WriteLine("Please be more specific.");
                    return "";
                }
                action = actionList;
                foreach (String s in indexes.Keys)
                {
                    if (action.Contains("[@" + s + "]"))
                    {
                        action = action.Replace("[@" + s + "]", command[indexes[s]]);
                    }
                }


            }
            return action;
        }

        private void doCommand(String action)
        {
            if (action.StartsWith("<?")) // If int
            {
                String[] actions = action.Split(',');
                actions[0] = actions[0].Replace("<?", "").Replace(">", "");
                actions[1] = actions[1].Replace("(+", "").Replace(")", "");
                actions[2] = actions[2].Replace("(-", "").Replace(")", "");

                String[] condition = actions[0].Split('=');
                int num = 0;
                Int32.TryParse(condition[1], out num);
                if (states[condition[0]] == num)
                {
                    doCommand(actions[1]);
                }
                else
                {
                    doCommand(actions[2]);
                }
            }
            else if (action.StartsWith("{?")) // if item in room
            {
                String[] actions = action.Split(',');

                actions[0] = actions[0].Replace("{?", "").Replace("}", "");
                actions[1] = actions[1].Replace("(+", "").Replace(")", "");
                actions[2] = actions[2].Replace("(-", "").Replace(")", "");

                if (items[current].Contains(actions[0]))
                {
                    doCommand( actions[1] );
                }
                else
                {
                    doCommand(actions[2]);
                }
            }
            else if (action.StartsWith("[?")) // if player has item
            {
                String[] actions = action.Split(',');
                actions[0] = actions[0].Replace("[?", "").Replace("]", "");
                actions[1] = actions[1].Replace("(+", "").Replace(")", "");
                actions[2] = actions[2].Replace("(-", "").Replace(")", "");

                if (playerItems.Contains(actions[0]))
                {
                    doCommand(actions[1]);
                }
                else
                {
                    doCommand(actions[2]);
                }
            }
            else if (action.Contains("|->|")) //Multiple commands
            {
                String[] commands = Regex.Split(action, "\\|->\\|");
                foreach (String s in commands)
                {
                    doCommand(s);
                }
                return;
            }
            else if ((action.StartsWith("[")) && (action.EndsWith("]")) && (!((action.StartsWith("[@")))))
            {
                current = action.Replace("[", "").Replace("]", "");
                currentFile = parseJSONFile(current + ".json");
                getAllStatechangers(currentFile);
                if (currentFile.ContainsKey("title"))
                {
                    Console.Title = currentFile["title"];
                    Console.WriteLine(currentFile["title"]);
                }
                

                if (!items.ContainsKey(current))
                {
                    if (currentFile.ContainsKey("ITEMS"))
                    {
                        List<dynamic> itemsTemp = new List<dynamic>();
                        foreach (String s in currentFile["ITEMS"])
                        {
                            itemsTemp.Add(s);
                        }
                        items.Add(current, itemsTemp);
                    }
                    else
                    {
                        items.Add(current, new List<dynamic>());
                    }
                }

                if (currentFile.ContainsKey("description"))
                {
                    printDescription(current);
                }
                return;
            }
            else if ((action.StartsWith("{")) && (action.EndsWith("}")) && (!((action.StartsWith("{@"))))) 
            {
                String n = action;
                n = n.Replace("{", "").Replace("}", "");
                evalCommand(new String[] { n });
            }

            else if (action.Equals("{@inv}"))
            {
                foreach (String s in items[current])
                {
                    if (itemD.ContainsKey(s))
                    {
                        Console.WriteLine(itemD[s]);
                    }
                }
            }
            else if ((action.StartsWith("<+")) && (action.EndsWith(">"))) //Player items add
            {
                action = action.Replace("<+", "");
                action = action.Replace(">", "");

                playerItems.Add(action);
            }

            else if ((action.StartsWith("<-")) && (action.EndsWith(">"))) //player items remove
            {
                action = action.Replace("<-", "");
                action = action.Replace(">", "");

                playerItems = safeRemove(playerItems, action);
            }
            else if ((action.StartsWith("<!+")) && (action.EndsWith(">"))) // Room items add
            {
                action = action.Replace("<!+", "");
                action = action.Replace(">", "");

                items[current].Add(action);
            }

            else if ((action.StartsWith("<!-")) && (action.EndsWith(">"))) // Room items remove
            {
                action = action.Replace("<!-", "");
                action = action.Replace(">", "");
                items[current] = safeRemove(items[current], action);
            }
            else if (action.StartsWith("<#")) // Change int
            {
                String[] actions = Regex.Split(action, "==");
                actions[0] = actions[0].Replace("<#", "").Replace(">", "");
                int num = 0;
                Int32.TryParse(actions[1], out num);
                states[actions[0]] = num;
            }
            else
            {
                String output = action;
                output = processState(output, currentFile);
                String[] o = Regex.Split(output,"/n");
                foreach (String s in o)
                {
                    Console.WriteLine(s);
                }
            }
        }

        private void startUp()
        {
            main = parseJSONFile("main.json");
            startroom = (String)main["start"];
            current = startroom;
            itemD = parseJSONFile("items.json");
        }

        private String[] getCommand()
        {
            Console.Write(">");
            String s = Console.ReadLine().ToLower();
            return s.Split(' ');
        }

        private Dictionary<String, dynamic> parseJSONFile(String file)
        {
            String[] text = File.ReadAllLines(file);
            String t = "";
            foreach(String s in text)
            {
                t += s;
            }
            var jss = new JavaScriptSerializer();
            return jss.Deserialize<Dictionary<String, dynamic>>(t);
        }

        private void getAllStatechangers(Dictionary<String, dynamic> dict)
        {
            foreach (String s in dict.Keys)
            {
                if (s.StartsWith("[@") && s.EndsWith("]"))
                {
                    if (!states.ContainsKey(s))
                    {
                        states.Add(s, 0);
                    }
                }
            }
        }

        private String processState(String input, Dictionary<String, dynamic> dict)
        {
            foreach (String s in states.Keys)
            {
                if (input.Contains(s))
                {
                    input = input.Replace(s, dict[s][states[s]]);
                }
            }
            return input;
        }

        private List<String> safeRemove(List<String> list, String s)
        {
            if (list.Contains(s))
            {
                list.Remove(s);
            }

            return list;
        }

        private List<dynamic> safeRemove(List<dynamic> list, String s)
        {
            if (list.Contains(s))
            {
                list.Remove(s);
            }

            return list;
        }

        private void printDescription(String file)
        {
            String d = parseJSONFile(file + ".json")["description"];
            d = processState(d,parseJSONFile(file+".json"));

            String itemList = "";

            if (parseJSONFile(file + ".json").ContainsKey("ITEMS"))
            {
                foreach (String s in items[file])
                {
                    itemList += itemD[s] + "/n";
                }
            }

            d = d.Replace("{@inv}", itemList);

            String[] da = Regex.Split(d,"/n");

            foreach (String line in da)
            {
                Console.WriteLine(line);
            }
        }
    }
}